FROM node:14

WORKDIR /usr/src/node-write-image

COPY ./package.json /usr/src/node-write-image

RUN npm install

COPY . /usr/src/node-write-image

EXPOSE 3005

CMD ["npm", "start"]