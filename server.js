import express, { response } from "express"
import bodyParser from "body-parser"
import axios from "axios"
import dotenv, { config } from "dotenv"
import path from 'path'
import fs from 'fs'
import randomstring from 'randomstring'
import sharp from 'sharp'
import imageToBase64 from 'image-to-base64'

const app = express()
const host = process.env.PORT || "0.0.0.0"
const port = process.env.PORT || 3005


app.use(express.json({limit: '50mb', extended: true}))
app.use(express.urlencoded({limit: '50mb', extended: true}))
app.use(bodyParser.json({limit: '50mb', extended: true}))
app.use(bodyParser.urlencoded({limit: '50mb', extended: true}))

dotenv.config()

// let fileObject = path.parse(__filename)
// console.log("fileObject =====>", fileObject)
// let pathObj = path.join(__dirname, fileObject.base)
// console.log("pathObj =====>", pathObj)

let pathObject = path.join(__dirname)

let file_req_img = randomstring.generate(5) + ".jpg"
// console.log("file_req_img =====>", file_req_img)
let filename = randomstring.generate(5) + ".jpg"
// console.log("filename =====>", filename)


const genImage = async(data) => {

    if (Object.keys(data).includes('image') === true){
        console.log("Array_key :", Object.keys(data))
        console.log("Object.keys(data) :", Object.keys(data).includes('image'))
        fs.writeFile(pathObject + "/image/" + file_req_img, data.image, {encoding: 'base64'}, (err)=>{if (err) throw new Error(err)})
        return {message : "create file success", filename : filename}
    }else{
        console.log("Array_key :", Object.keys(data))
        console.log("Object.keys(data) :", Object.keys(data).includes('image'))
        fs.writeFile(pathObject + "/image/" + filename, data.image_base64, {encoding: 'base64'}, (err)=>{if (err) throw new Error(err)})
        return {message : "create file success", filename : file_req_img}
    }
}


app.post('/api/v1/compare/face', async(req, res)=>{ 
    try{ 
        let data = req.body

        let config_get_image = {
            method: 'post',
            url: process.env.URL_FACE,
            headers: {
                'Content-Type': 'application/json'
            },
            data: JSON.stringify({"image_id": data.id})
        }
        let get_image = await axios(config_get_image).then(response=>response).catch(err=>{throw new Error(err.message)})

        let decode_body_img = await genImage(data)
        let decode_img_base64 = await genImage(get_image.data)


        if ((decode_img_base64.message && decode_body_img.message) === "create file success"){

            fs.readdir(pathObject + "/image", async(err, file) =>{
                if(err) throw new Error(err)

                let face_base64img = Object.assign([], [])

                for (let each_file of file){

                    let resharpfile = randomstring.generate(5) + ".jpg"

                    let tmp_input_path = path.join(pathObject + "/image", each_file)  
                    let tmp_output_path = path.join(pathObject + "/image", resharpfile)

                    // ทำการ resize รูป
                    await sharp(tmp_input_path).resize({ height: 600, width: 400 }).toFile(tmp_output_path)
                    .then(response => response).catch(err => err)

                    // ยังติด buck เรื่อง ที่รูป ถูก encode เเล้ว ซ้ำกัน ได้ data ทั้งสองชุดเหมือนกัน
                    let convert_img = fs.readFileSync(path.join(pathObject + "/image", resharpfile))
                    let read_buffer = Buffer.from(convert_img).toString('base64')
                    console.log("read_buffer :", read_buffer)
                    
                }
                
                // stage เปรียบเทียบหน้า
                if (JSON.stringify(face_base64img) !== "[]"){
                    console.log("stage compare")

                    let new_data = {"face_base64img_1": face_base64img[0].img, "face_base64img_2": face_base64img[1].img}
        
                    let config_compare_image ={
                        method: 'post',
                        url: 'https://api.manageai.co.th/gateway/api/v1/forward',
                        headers: { 
                        'authorization': process.env.API_COMPARE_TOKEN, 
                        'Content-Type': 'application/json'
                        },
                        data : JSON.stringify(new_data)
                    }
                    let accuracy = await axios(config_compare_image)
                    .then(response=>response)
                    .catch(err=> {throw new Error(err.message)})

                    if (parseFloat(accuracy.data.data.score) >= parseFloat("60.0")){
                        console.log("stage pass")
                        fs.readdir(pathObject + "/image", (err, allfile)=>{
                            if (err) throw new Error(err)
                            for (let files of allfile){
                                fs.unlink(path.join(pathObject + "/image", files), (err)=>{
                                    if (err) throw new Error(err)
                                    console.log('File deleted!')
                                })
                            }
                        })
                        res.status(accuracy.status).json({message: accuracy.data.data.text, status: accuracy.data.data.message, data:{accuracy:accuracy.data.data.score, status: "pass"}})
                    }else{
                        console.log("stage not pass")
                        fs.readdir(pathObject + "/image", (err, allfile)=>{
                            if (err) throw new Error(err)
                            for (let files of allfile){
                                fs.unlink(path.join(pathObject + "/image", files), (err)=>{
                                    if (err) throw new Error(err)
                                    console.log('File deleted!')
                                })
                            }
                        })
                        res.status(accuracy.status).json({message: "Not the same person", status: "fail", data: {accuracy:accuracy.data.data.score, status: "not pass"}})
                    }
                    
                }else{
                    throw new Error("Error face_base64img please check file")
                }
            })
        }
    }catch(err){
        console.log(err)
        res.status(400).json({message: err.message, status: "fail", data: ""})
    }
})


app.listen(port, host, ()=>{
    console.log(`Starting node.js on ${host}:${port}`)
})